import React, { Component } from 'react';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Preloader from './components/Preloader';
import Seat from './components/Seat';


import { getSeats, bookSeat, unbookSeat } from './api';

import styled from 'styled-components';


const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  margin-right: auto;
  margin-left: auto;
  width: 95%;
  min-width: 320px;
  height: 100vh;
`;

const SeatList = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: space-around;
  align-items: flex-start;

  width: 100%;
`;

class App extends Component {
  constructor() {
    super();

    this.state = {
      isLoading: true,
      data: null,
      token: null,
    }
  }

  componentDidMount() {
    const secret = localStorage.getItem('secretKey');
    if (secret) {
      this.setState({ token: secret }, () => {
        this.updateSeats()
      })
    } else {
      this.updateSeats()
    }

  }

  updateSeats = () => {
    const { token } = this.state;
    getSeats()
      .then(res => {
        if (res.headers.token && res.data) {
          this.setState({ data: res.data }, () => {
            if (!token) {
              this.setState({ token: res.headers.token })
              localStorage.setItem('secretKey', res.headers.token)
            }
          })
        } else {
          toast('data format error')
        }
      })
      .catch(error => toast(error.message))
      .finally(() => this.setState({ isLoading: false }))
  }

  tryBook = (name) => {
    const { token } = this.state;
    this.setState({ isLoading: true })
     bookSeat(name, token)
     .then(res => {
       if (res.data.success === true) {
          toast('successfully booked')
          this.updateSeats()
       } else {
         toast('booking was unsuccessful')
       }
     })
     .catch(error => toast(error.message))
     .finally(() => this.setState({ isLoading: false }))
  }

  tryUnbook = (name) => {
    const { token } = this.state;
    this.setState({ isLoading: true })
     unbookSeat(name, token)
     .then(res => {
       if (res.data.success === true) {
          toast('successfully unbooked')
          this.updateSeats()
       } else {
         toast('unbooking was unsuccessful: another user booked it')
       }
     })
     .catch(error => toast(error.message))
     .finally(() => this.setState({ isLoading: false }))
  }

  render() {
    const { isLoading, data } = this.state;
    let Seats;
    if (data) {
      Seats = data.map(item => {
        const { seatNumber, status } = item;
        let clickHandler;

        if (status === 'free') {
          clickHandler = () => this.tryBook(seatNumber)
        } else if (status === 'booked') {
          clickHandler = () => this.tryUnbook(seatNumber)
        }

        return <Seat key={seatNumber} name={seatNumber} status={status} clickHandler={clickHandler} />
      });
    }


    return (
      <div className="App">
        {isLoading ? (
          <Preloader />
        ) : (
          <Wrapper>
            <SeatList>
              {Seats}
            </SeatList>
          </Wrapper>
        )}

        <ToastContainer />
      </div>
    );
  }
}

export default App;
