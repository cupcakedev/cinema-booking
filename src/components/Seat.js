import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const SeatContainer = styled.button`
  border: 1px solid grey;
  margin: 10px;
  padding: 15px 20px;
  font-size: 16px;

  background: ${props => props.status === 'booked' ? "black" : "white"};
  color: ${props => props.status === 'booked' ? "white" : "black"};

  :hover {
    opacity: 0.8;
    cursor: pointer;
  }
`;

const Seat = ({
  name,
  status,
  clickHandler,
}) => {

  return (
    <SeatContainer status={status} onClick={clickHandler}>
      <p>{name}</p>
    </SeatContainer>
  )
}

export default Seat;

Seat.propTypes = {
  name: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  clickHandler: PropTypes.func.isRequired,
}
