import React from 'react';
import styled from 'styled-components';

const Fader = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  color: #333;
  background-color: rgba(255, 255, 255, 0.9);
`;

const Text = styled.div`
  font-size: 25px;

`;

const LoadContainer = styled.div`
  width: 30%;
  text-align: center;
`;


const Preloader = () => {
  return (
    <Fader>
      <LoadContainer>
        <Text style={{ marginTop: '15px' }}>Loading...</Text>
      </LoadContainer>
    </Fader>
  )
}

export default Preloader;
