import axios from 'axios';


const API_PATH = 'http://37.194.33.150:3000/api/';

export const getSeats = () =>
  axios.get(`${API_PATH}seats/`)

export const bookSeat = (name, token) =>
  axios.put(`${API_PATH}seats/${name}`, null, { headers: { Token: token } })

export const unbookSeat = (name, token) =>
  axios.put(`${API_PATH}seats/${name}`, {'newStatus': 'free'}, { headers: { Token: token } })
